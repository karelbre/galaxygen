// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#pragma once

#include <galaxygen_export.h>
#include <BaseShape.h>

namespace galaxy_generator
{
	/**
	 * Data structure for sphere shape
	 */
	struct GALAXYGEN_EXPORT SphereData : BaseShapeData
	{
		float size_;
		float density_deviation_;
		float density_mean_;
		float deviation_x_;
		float deviation_y_;
		float deviation_z_;

		/**
		 * Constructor
		 */
		SphereData() : BaseShapeData(glm::vec2(1000, 10000)), size_(0.0f), density_deviation_(0.0f),
			density_mean_(0.0f), deviation_x_(0.0f), deviation_y_(0.0f),
			deviation_z_(0.0f)
		{
			data_type_ = SPHERE;
		}

		/**
		 * Constructor
		 */
		SphereData(const SphereData& data) : BaseShapeData(data.temp_range_)
		{
			data_type_ = SPHERE;
			size_ = data.size_;
			density_mean_ = data.density_mean_;
			density_deviation_ = data.density_deviation_;
			deviation_x_ = data.deviation_x_;
			deviation_y_ = data.deviation_y_;
			deviation_z_ = data.deviation_z_;
		}

		/**
		 * Constructor
		 */
		SphereData(
			glm::vec2 temp_range,
			float size,
			float density_deviation = 0.000001f,
			float density_mean = 0.00025f,
			float deviation_x = 0.0000025f,
			float deviation_y = 0.0000025f,
			float deviation_z = 0.0000025f) : BaseShapeData(temp_range)
		{
			data_type_ = SPHERE;
			size_ = size;
			density_mean_ = density_mean;
			density_deviation_ = density_deviation;
			deviation_x_ = deviation_x;
			deviation_y_ = deviation_y;
			deviation_z_ = deviation_z;
		}
	};

	/**
	 * Class provides generate sphere galaxy shape
	 */
	class GALAXYGEN_EXPORT Sphere : public BaseShape
	{
	public:
		Sphere(std::shared_ptr<SphereData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen);
		void SetData(std::shared_ptr<SphereData> data);
		void Generate() override;
		void Offset(glm::vec3 offset) const;
	};
}
