// AUTHOR: Karel Brezina
// DATE: 25.11.2017

#pragma once

#include <galaxygen_export.h>
#include <vector>
#include <BaseShape.h>
#include <Heatmap.h>

namespace galaxy_generator
{
	/**
	 * Base class for defining galaxy generator class 
	 */
	class GALAXYGEN_EXPORT BaseGenerator
	{
	public:
		virtual ~BaseGenerator() = default;
		virtual std::shared_ptr<std::vector<SunList>> Generate(galaxy_type type) = 0;
		virtual std::shared_ptr<std::vector<SunList>> Generate(const HeatmapGenData& gen_data) = 0;
		virtual bool SetGeneratorSettings(std::shared_ptr<BaseShapeData>) { return false; }
		virtual std::shared_ptr<BaseExtraInfo> GetExtraInfo(galaxy_type type) = 0;
	};
}