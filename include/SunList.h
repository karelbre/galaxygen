#pragma once

#include <galaxygen_export.h>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace galaxy_generator
{
	/**
	 * Class represents sun list entity
	 */
	class SunList
	{
	public:
		SunList(glm::vec3 position, std::string name, float temp = 0);
		glm::vec3 GetPosition() const;
		std::string GetName() const;
		float GetTemperature() const;
		void Offset(glm::vec3 offset);
		void Scale(glm::vec3 scale);
		void Swirl(glm::vec3 axis, float amount);

	protected:
		glm::vec3 position_;
		std::string name_;
		float temp_;
	};

	/**
	* Structure encapsulate sun list data
	*/
	struct SunListData
	{
		/**
		 * Constructor
		 */
		SunListData(glm::vec3 position, float temperature)
		{
			position_ = position;
			temperature_ = temperature;
		}

		glm::vec3 position_;
		float temperature_;
	};
}
