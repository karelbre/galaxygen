// AUTHOR: Karel Brezina
// DATE: 25.11.2017

#pragma once

#include <galaxygen_export.h>
#include <BaseGenerator.h>
#include <Sphere.h>
#include <Grid.h>
#include <Spiral.h>
#include <Cluster.h>

namespace galaxy_generator
{
	/**
	 * Class provides galaxy generate by heatmap or predefined shape
	 */
	class GALAXYGEN_EXPORT DefaultGenerator : public BaseGenerator
	{
	public:
		DefaultGenerator(std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen);
		std::shared_ptr<std::vector<SunList>> Generate(galaxy_type type) override;
		std::shared_ptr<std::vector<SunList>> Generate(const HeatmapGenData& gen_data) override;
		bool SetGeneratorSettings(std::shared_ptr<BaseShapeData> data) override;
		std::shared_ptr<BaseExtraInfo> GetExtraInfo(galaxy_type type) override;

	protected:
#pragma warning(push)
#pragma warning(disable:4251)
		std::shared_ptr<BaseExtraInfo> heatmap_extra_info_;
		glm::vec2 temp_range_;
		std::shared_ptr<Random> rnd_;
		std::shared_ptr<NameGenerator> name_gen_;
#pragma warning(pop)
		Sphere sphere_;
		Grid grid_;
		Spiral spiral_;
		Cluster cluster_;
	};
}
