// AUTHOR: Karel Brezina
// DATE: 2.10.2017

#pragma once

#include <galaxygen_export.h>
#include <BaseShape.h>

namespace galaxy_generator
{
	/**
	 * Data structure for grid shape
	 */
	struct GALAXYGEN_EXPORT GridData : BaseShapeData
	{
		float size_;
		float spacing_;

		/**
		 * Constructor
		 */
		GridData() : BaseShapeData(glm::vec2(1000, 10000)), size_(0.0f), spacing_(0.0f)
		{
			data_type_ = GRID;
		}

		/**
		 * Constructor
		 */
		GridData(glm::vec2 temp_range, float size, float spacing) : BaseShapeData(temp_range)
		{
			data_type_ = GRID;
			size_ = size;
			spacing_ = spacing;
		}
	};

	/**
	 * Class provides generate grid glalaxy shape
	 */
	class GALAXYGEN_EXPORT Grid : public BaseShape
	{
	public:
		Grid(std::shared_ptr<GridData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen);
		void SetData(std::shared_ptr<GridData> data);
		void Generate() override;
	};
}