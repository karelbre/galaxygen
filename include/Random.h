// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#pragma once

#include <galaxygen_export.h>
#include <algorithm>
#include <Random>

namespace galaxy_generator
{
	/**
	 * Class provides basic set of random functions (uniform, normal distribution)
	 */
	class GALAXYGEN_EXPORT Random
	{
	public:
		Random();
		float GetRandomNormalRange(float deviation, float mean, float min, float max);
		float GetRandomNormalRange(float deviation, float mean, int min, int max);
		float GetRandomNormal(float deviation, float mean);
		int GetRandomUniformRange(int min, int max);
		float GetRandomUniformRange(float min, float max);
		float GetRandomUniform();

	private:
#pragma warning(push)
#pragma warning(disable:4251)
		std::mt19937 rnd_generator_;
		std::uniform_real_distribution<float> dis_uniform_;
#pragma warning(pop)
	};
}