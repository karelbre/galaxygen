// AUTHOR: Karel Brezina
// DATE: 19.6.2017

#pragma once

#include <galaxygen_export.h>
#include <memory>

#include <glm/glm.hpp>
#include <BaseShape.h>

namespace galaxy_generator
{
	/**
	 * Data structure for heatmap generate
	 */
	struct GALAXYGEN_EXPORT Heatmap
	{
		Heatmap();
		bool IsLoaded() const;
		void SetSourceImage(int width, int height, glm::vec4** pixels);
		int GetWidth() const;
		int GetHeight() const;
		float GetTempProb(int x, int y) const;
		float GetGenProb(int x, int y) const;
		std::shared_ptr<Heatmap> GetCopy() const;
		bool EraseProbAround(glm::ivec2 startin_point, int radius) const;

	private:
		bool is_loaded_;
		int width_;
		int height_;
		glm::vec4** pixels_;
	};

	/**
	* Data structure for additional heatmap generate info
	*/
	struct HeatmapGenData
	{
		/**
		 * Constructor
		 */
		HeatmapGenData(std::shared_ptr<Heatmap> heatmap, glm::vec3 galaxy_size, glm::vec2 temp_range,
			float height_deviation, float temp_deviation, int minimal_distance)
		{
			galaxy_size_ = galaxy_size;
			temp_range_ = temp_range;
			height_deviation_ = height_deviation;
			temp_deviation_ = temp_deviation;
			minimal_distance_ = minimal_distance;
			heatmap_ = heatmap;
		}

		glm::vec3 galaxy_size_;
		glm::vec2 temp_range_;
		float height_deviation_;
		float temp_deviation_;
		int minimal_distance_;
		std::shared_ptr<Heatmap> heatmap_;
	};
}
