// AUTHOR: Karel Brezina
// DATE: 2.10.2017

#pragma once

#include <galaxygen_export.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <BaseShape.h>

namespace galaxy_generator
{
	/**
	 * Data structure for spiral shape
	 */
	struct GALAXYGEN_EXPORT SpiralData : BaseShapeData
	{
		float size_;
		float spacing_;

		int minimum_arms_;
		int maximum_arms_;

		float cluster_count_deviation_;
		float cluster_center_deviation_;

		float min_arm_cluster_scale_;
		float max_arm_cluster_scale_;
		float arm_cluster_scale_deviation_;

		float swirl_;

		float center_cluster_scale_;
		float center_cluster_density_mean_;
		float center_cluster_density_deviation_;
		float center_cluster_size_deviation_;

		float center_cluster_position_deviation_;
		float center_cluster_count_deviation_;
		float center_cluster_count_mean_;

		float central_void_size_mean_;
		float central_void_size_deviation_;

		/**
		 * Constructor
		 */
		SpiralData(glm::vec2 temp_range = glm::vec2(1000, 10000), float size = 750, float spacing = 5, int minimum_arms = 3, int maximum_arms = 7, 
			float cluster_count_deviation = 0.35f, float cluster_center_deviation = 0.2f, float min_arm_cluster_scale = 0.02f,
			float max_arm_cluster_scale = 0.1f, float arm_cluster_scale_deviation = 0.02f, float swirl = M_PI * 4,
			float center_cluster_scale = 0.19f, float center_cluster_density_mean = 0.00005f, 
			float center_cluster_density_deviation = 0.000005, float center_cluster_size_deviation = 0.00125f, 
			float center_cluster_position_deviation = 0.075f, float center_cluster_count_deviation = 3, 
			float center_cluster_count_mean = 20, float central_void_size_mean = 25, float central_void_size_deviation = 7)
			: BaseShapeData(temp_range)
		{
			data_type_ = SPIRAL;
			size_ = size;
			spacing_ = spacing;

			minimum_arms_ = minimum_arms;
			maximum_arms_ = maximum_arms;

			cluster_count_deviation_ = cluster_count_deviation;
			cluster_center_deviation_ = cluster_center_deviation;

			min_arm_cluster_scale_ = min_arm_cluster_scale;
			arm_cluster_scale_deviation_ = arm_cluster_scale_deviation;
			max_arm_cluster_scale_ = max_arm_cluster_scale;

			swirl_ = swirl;

			center_cluster_scale_ = center_cluster_scale;
			center_cluster_density_mean_ = center_cluster_density_mean;
			center_cluster_density_deviation_ = center_cluster_density_deviation;
			center_cluster_size_deviation_ = center_cluster_size_deviation;

			center_cluster_count_mean_ = center_cluster_count_mean;
			center_cluster_count_deviation_ = center_cluster_count_deviation;
			center_cluster_position_deviation_ = center_cluster_position_deviation;

			central_void_size_mean_ = central_void_size_mean;
			central_void_size_deviation_ = central_void_size_deviation;
		}
	};

	/**
	 * Class provides generate spiral galaxy shape
	 */
	class GALAXYGEN_EXPORT Spiral : public BaseShape
	{
	public:
		Spiral(std::shared_ptr<SpiralData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen);
		void SetData(std::shared_ptr<SpiralData> data);
		void Generate() override;

	protected:
		void GenerateBackgroundSunList() const;
		void GenerateCenter() const;
		void GenerateArms() const;
	};
}
