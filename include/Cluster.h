// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#pragma once

#include <galaxygen_export.h>
#include <BaseShape.h>

namespace galaxy_generator
{
	/**
	 * Data structure for cluster shape
	 */
	struct GALAXYGEN_EXPORT ClusterData : BaseShapeData
	{
		BaseShape* basis_;
		int basis_type_;
		float count_deviation_;
		float count_mean_;
		float deviation_x_;
		float deviation_y_;
		float deviation_z_;

		/**
		 * Constructor
		 */
		ClusterData() : BaseShapeData(glm::vec2(1000, 10000)), basis_(nullptr), basis_type_(-1), count_deviation_(0.0f),
			count_mean_(0.0f), deviation_x_(0.0f), deviation_y_(0.0f),
			deviation_z_(0.0f) 
		{
			data_type_ = CLUSTER;
		}

		/**
		 * Constructor
		 */
		ClusterData(
			glm::vec2 temp_range,
			BaseShape* basis,
			int basis_type,
			float count_deviation = 0.000001f,
			float count_mean = 0.0000025f,
			float deviation_x = 0.0000025f,
			float deviation_y = 0.0000025f,
			float deviation_z = 0.0000025f) : BaseShapeData(temp_range)
		{
			data_type_ = CLUSTER;
			basis_ = basis;
			basis_type_ = basis_type;
			count_deviation_ = count_deviation;
			count_mean_ = count_mean;
			deviation_x_ = deviation_x;
			deviation_y_ = deviation_y;
			deviation_z_ = deviation_z;
		}
	};

	/**
	 * Data structure for cluster extra info
	 */
	struct GALAXYGEN_EXPORT ClusterExtraInfo : BaseExtraInfo
	{
		int basis_amount_;

		/**
		* Constructor
		* @param basis_amount amount of cluster base shape
		*/
		ClusterExtraInfo(int basis_amount) : BaseExtraInfo()
		{
			basis_amount_ = basis_amount;
		}

		/**
		 * Constructor
		 * @param galaxy_size_min AABB corner of generated galaxy
		 * @param galaxy_size_max opposite AABB corner of generated galaxy
		 * @param basis_amount amount of cluster base shape
		 */
		ClusterExtraInfo(glm::vec3 galaxy_size_min, glm::vec3 galaxy_size_max, int basis_amount)
			: BaseExtraInfo(galaxy_size_min, galaxy_size_max)
		{
			basis_amount_ = basis_amount;
		}
	};

	/**
	 * Class provides generate cluster galaxy shape
	 */
	class GALAXYGEN_EXPORT Cluster : public BaseShape
	{
	public:
		Cluster(std::shared_ptr<ClusterData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen);
		void SetData(std::shared_ptr<ClusterData> data);
		void Generate() override;
	};
}
