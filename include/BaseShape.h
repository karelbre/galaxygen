// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#pragma once

#include <galaxygen_export.h>
#include <vector>
#include <memory>

#include <SunList.h>
#include <Random.h>
#include <NameGenerator.h>

namespace galaxy_generator
{
	/**
	 * Enum specifying galaxy shape
	 */
	enum galaxy_type
	{
		UNKNOWN = 0,
		SPHERE = 1,
		SPIRAL = 2,
		GRID = 3,
		CLUSTER = 4,
	};

	/**
	 * Base structure for shape data
	 */
	struct GALAXYGEN_EXPORT BaseShapeData
	{
		galaxy_type data_type_;
#pragma warning(push)
#pragma warning(disable:4251)
		glm::vec2 temp_range_;
#pragma warning(pop)

		/**
		 * Constructor
		 * @param temp_range range of sun list temperature
		 */
		BaseShapeData(glm::vec2 temp_range)
		{
			data_type_ = UNKNOWN;
			temp_range_ = temp_range;
		}
	};

	/**
	 * Base structure for extra info
	 */
	struct GALAXYGEN_EXPORT BaseExtraInfo
	{
#pragma warning(push)
#pragma warning(disable:4251)
		glm::vec3 galaxy_size_min_;
		glm::vec3 galaxy_size_max_;
#pragma warning(pop)

		/**
		 * Constructor
		 */
		BaseExtraInfo()
		{
			const float inf = std::numeric_limits<float>::infinity();
			galaxy_size_min_ = glm::vec3(inf, inf, inf);
			galaxy_size_max_ = glm::vec3(-inf, -inf, -inf);
		}

		/**
		 * Constructor
		 * @param galaxy_size_min AABB corner of generated galaxy
		 * @param galaxy_size_max opposite AABB corner of generated galaxy
		 */
		BaseExtraInfo(glm::vec3 galaxy_size_min, glm::vec3 galaxy_size_max)
		{
			galaxy_size_min_ = galaxy_size_min;
			galaxy_size_max_ = galaxy_size_max;
		}
	};

	/**
	 * Base class for defining galaxy shape generate
	 */
	class GALAXYGEN_EXPORT BaseShape
	{
	public:
		BaseShape(glm::vec2 temp_range, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen);
		virtual ~BaseShape() { }
		virtual void Generate() = 0;
		virtual std::shared_ptr<std::vector<SunList>> GetSunList() const;
		virtual std::shared_ptr<BaseExtraInfo> GetExtraInfo() const;
		void SetVars(glm::vec2 temp_range, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen);

	protected:
#pragma warning(push)
#pragma warning(disable:4251)
		std::shared_ptr<std::vector<SunList>> sun_lists_;
		std::shared_ptr<BaseExtraInfo> extra_info_;
		std::shared_ptr<Random> rnd_;
		std::shared_ptr<NameGenerator> name_gen_;
		std::shared_ptr<BaseShapeData> data_;
#pragma warning(pop)
	};
}