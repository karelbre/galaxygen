// AUTHOR: Karel Brezina
// DATE: 19.6.2017

#pragma once

#include <galaxygen_export.h>
#include <BaseGenerator.h>
#include <DefaultGenerator.h>
#include <Heatmap.h>
#include <NameGenerator.h>
#include <Random.h>

namespace galaxy_generator
{
	/**
	 * Class provides entry point for generating galaxies
	 */
	class GALAXYGEN_EXPORT GalaxyGen
	{
	public:
		GalaxyGen();
		void SetGenerator(const std::shared_ptr<BaseGenerator> generator);
		std::shared_ptr<std::vector<SunListData>> Generate(const galaxy_type& type, const std::shared_ptr<BaseGenerator> generator = nullptr);
		std::shared_ptr<std::vector<SunListData>> Generate(const HeatmapGenData& gen_data, const std::shared_ptr<BaseGenerator> generator = nullptr);
		std::shared_ptr<BaseExtraInfo> GetExtraInfo(galaxy_type type) const;
		bool SetGeneratorSettings(std::shared_ptr<BaseShapeData> data) const;
		
	protected:
		std::shared_ptr<std::vector<SunListData>> Parse(const std::shared_ptr<std::vector<SunList>> sun_list) const;

#pragma warning(push)
#pragma warning(disable:4251)
		std::shared_ptr<NameGenerator> name_gen_;
		std::shared_ptr<Random> rnd_;
		std::shared_ptr<BaseGenerator> generator_;
#pragma warning(pop)
	};
}
