﻿// AUTHOR: Karel Brezina
// DATE: 19.6.2017

#pragma once

#include <galaxygen_export.h>
#include <string>
#include <vector>
#include <memory>

#include <Random.h>

namespace galaxy_generator
{
	/**
	 * Class provides name generator
	 */
	class GALAXYGEN_EXPORT NameGenerator
	{
	public:
		NameGenerator(std::shared_ptr<Random> rnd);
		bool LoadNamesFromFile(std::string filename);
		std::string GetName();

	private:
#pragma warning(push)
#pragma warning(disable:4251)
		std::vector<std::string> first_names_;
		std::vector<std::string> second_names_;
		std::shared_ptr<Random> rnd_;
#pragma warning(pop)
		bool is_loaded_;
	};
}
