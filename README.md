======================
Subject: Master thesis
======================
Name: GalaxyGen
======================
Last updated: 29.4.2018
======================
Author: Karel Březina
======================
Description: 

Galaxy generator library for games. 
CMake (version 3.8.0 or higher) is needed for create build solution.
Dependencies => GLM library

You can choose between Debug and Release mode.
If you want to create standalone app using this library check IS_STANDALONE.

Git repository: https://bitbucket.org/karelbre/galaxygen