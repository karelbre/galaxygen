#include <SunList.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 */
	SunList::SunList(glm::vec3 position, std::string name, float temp)
	{
		position_ = position;
		name_ = name;
		temp_ = temp;
	}

	/**
	 * Get sun list position
	 * @return Position
	 */
	glm::vec3 SunList::GetPosition() const
	{
		return position_;
	}

	/**
	 * Gen sun list name
	 * @return Name
	 */
	std::string SunList::GetName() const
	{
		return name_;
	}

	/**
	 * Get sun list temperature
	 * @return Temperature
	 */
	float SunList::GetTemperature() const
	{
		return temp_;
	}

	/**
	 * Give offset to sun list position
	 * @param offset 
	 */
	void SunList::Offset(glm::vec3 offset)
	{
		position_ += offset;
	}

	/**
	 * Give scale to sun list position
	 */
	void SunList::Scale(glm::vec3 scale)
	{
		position_ *= scale;
	}

	/**
	 * Swirl sun list position around position zero
	 */
	void SunList::Swirl(glm::vec3 axis, float amount)
	{
		float d = glm::length(position_);
		float a = static_cast<float>(pow(d, 0.1f) * amount);
		position_ = glm::angleAxis(a, axis) * position_;
	}
}