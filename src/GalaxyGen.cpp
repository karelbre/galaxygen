#include <GalaxyGen.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 */
	GalaxyGen::GalaxyGen()
	{
		rnd_ = std::make_shared<Random>();
		name_gen_ = std::make_shared<NameGenerator>(rnd_);
		generator_ = std::make_shared<DefaultGenerator>(rnd_, name_gen_);
	}

	/**
	 * Set new generator
	 * @param generator object providing generating
	 */
	void GalaxyGen::SetGenerator(const std::shared_ptr<BaseGenerator> generator)
	{
		if (generator != nullptr)
		{
			generator_ = generator;
		}
	}

	/**
	 * Generate galaxy of set type
	 * @param type galaxy shape
	 * @param generator object providing generating
	 * @return Generated list of suns
	 */
	std::shared_ptr<std::vector<SunListData>> GalaxyGen::Generate(
		const galaxy_type& type, 
		const std::shared_ptr<BaseGenerator> generator)
	{
		SetGenerator(generator);
		const std::shared_ptr<std::vector<SunList>> sun_list = generator_->Generate(type);
		return Parse(sun_list);
	}

	/**
	 * Generate galaxy by heatmap
	 * @param gen_data generating options
	 * @param generator object providing generating
	 * @return Generated list of suns
	 */
	std::shared_ptr<std::vector<SunListData>> GalaxyGen::Generate(
		const HeatmapGenData& gen_data, 
		const std::shared_ptr<BaseGenerator> generator)
	{
		SetGenerator(generator);
		const std::shared_ptr<std::vector<SunList>> sun_list = generator_->Generate(gen_data);
		return Parse(sun_list);
	}

	/**
	 * Get extra info after generating process done
	 * @param type galaxy shape (UNKNOWN => Heatmap extra info)
	 * @return Extra info of generating process
	 */
	std::shared_ptr<BaseExtraInfo> GalaxyGen::GetExtraInfo(galaxy_type type) const
	{
		return generator_->GetExtraInfo(type);
	}

	/**
	 * Set attributes for generating process of specified galaxy shape
	 * @see SphereData, ClusterData, GridData, SpiralData
	 * @param data structure specifying galaxy shape's attributes
	 * @return Success of set
	 */
	bool GalaxyGen::SetGeneratorSettings(std::shared_ptr<BaseShapeData> data) const
	{
		return generator_->SetGeneratorSettings(data);
	}

	/**
	 * Process generated suns info to easily used data structure
	 * @see SunListData
	 * @param sun_list generated sun list objects
	 * @return Data structure list of generated sun list
	 */
	std::shared_ptr<std::vector<SunListData>> GalaxyGen::Parse(const std::shared_ptr<std::vector<SunList>> sun_list_vec) const
	{
		std::shared_ptr<std::vector<SunListData>> sun_list_data = std::make_shared<std::vector<SunListData>>(std::vector<SunListData>());

		if (sun_list_vec != nullptr)
		{
			const int count = static_cast<int>(sun_list_vec->size());
			for (int i = 0; i < count; ++i)
			{
				SunList sun_list = sun_list_vec->at(i);
				const SunListData star_data(sun_list.GetPosition(), sun_list.GetTemperature());
				sun_list_data->emplace_back(star_data);
			}
		}

		return sun_list_data;
	}
}