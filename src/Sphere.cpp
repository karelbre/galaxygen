// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#include <Sphere.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 * @param data settings of sphere shape
	 * @param rnd random class object
	 * @param name_gen name generator class object
	 */
	Sphere::Sphere(std::shared_ptr<SphereData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen) 
		: BaseShape(data->temp_range_, rnd, name_gen)
	{
		SetData(data);
	}

	/**
	 * Set settings of sphere shape
	 */
	void Sphere::SetData(std::shared_ptr<SphereData> data)
	{
		data_ = data;
	}

	/**
	 * Generate galaxy of sphere shape
	 */
	void Sphere::Generate()
	{
		sun_lists_->clear();

		const std::shared_ptr<SphereData> sdata = std::static_pointer_cast<SphereData>(data_);
		const float density = glm::max(0.0f, rnd_->GetRandomNormal(sdata->density_deviation_, sdata->density_mean_));
		const int count_max = glm::max(0, static_cast<int>(sdata->size_ * sdata->size_ * sdata->size_ * density));
		if (count_max <= 0)
		{
			return;
		}

		const int count = rnd_->GetRandomUniformRange(0, count_max);
		for (int i = 0; i < count; ++i)
		{
			const glm::vec3 pos(
				rnd_->GetRandomNormal(sdata->deviation_x_*sdata->size_, 0.0f),
				rnd_->GetRandomNormal(sdata->deviation_y_*sdata->size_, 0.0f),
				rnd_->GetRandomNormal(sdata->deviation_z_*sdata->size_, 0.0f));

			const float temp = rnd_->GetRandomUniformRange(sdata->temp_range_.x, sdata->temp_range_.y);
			sun_lists_->emplace_back(SunList(pos, name_gen_->GetName(), temp));

			extra_info_->galaxy_size_min_ = glm::vec3(
				std::min(extra_info_->galaxy_size_min_.x, pos.x),
				std::min(extra_info_->galaxy_size_min_.y, pos.y),
				std::min(extra_info_->galaxy_size_min_.z, pos.z));
			extra_info_->galaxy_size_max_ = glm::vec3(
				std::max(extra_info_->galaxy_size_max_.x, pos.x),
				std::max(extra_info_->galaxy_size_max_.y, pos.y),
				std::max(extra_info_->galaxy_size_max_.z, pos.z));
		}
	}

	/**
	 * Offset position of sphere sun list
	 * @param offset offset position applied on every sun list
	 */
	void Sphere::Offset(glm::vec3 offset) const
	{
		const int max = static_cast<int>(sun_lists_->size());
		for (int i = 0; i < max; ++i)
		{
			(*sun_lists_)[i].Offset(offset);
		}
	}
}