// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#include <Cluster.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 * @param data settings of cluster shape
	 * @param rnd random class object
	 * @param name_gen name generator class object
	 */
	Cluster::Cluster(std::shared_ptr<ClusterData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen) : BaseShape(data->temp_range_, rnd, name_gen)
	{
		extra_info_ = std::make_shared<ClusterExtraInfo>(glm::vec3(), glm::vec3(), 0);
		SetData(data);
	}

	/**
	 * Set data variables to internal storage
	 * @param data set data
	 */
	void Cluster::SetData(std::shared_ptr<ClusterData> data)
	{
		data_ = data;
		data->basis_->SetVars(data->temp_range_, rnd_, name_gen_);
	}

	/**
	 * Generate sun list in cluster shape
	 */
	void Cluster::Generate()
	{
		// Reset
		sun_lists_->clear();
		extra_info_->galaxy_size_min_ = glm::vec3();
		extra_info_->galaxy_size_max_ = glm::vec3();

		std::shared_ptr<ClusterData> cdata = std::static_pointer_cast<ClusterData>(data_);
		const int count = glm::max(0, static_cast<int>(ceil(rnd_->GetRandomNormal(cdata->count_deviation_, cdata->count_mean_))));
		if (count <= 0)
		{
			return;
		}

		for (int i = 0; i < count; ++i)
		{
			const glm::vec3 center(
				rnd_->GetRandomNormal(cdata->deviation_x_, 0.0f),
				rnd_->GetRandomNormal(cdata->deviation_y_, 0.0f),
				rnd_->GetRandomNormal(cdata->deviation_z_, 0.0f));

			cdata->basis_->Generate();
			const std::shared_ptr<BaseExtraInfo> extra = cdata->basis_->GetExtraInfo();
			std::shared_ptr<std::vector<SunList>> sun_list = cdata->basis_->GetSunList();
			// Compute AABB borders
			extra_info_->galaxy_size_min_ = glm::vec3(
				std::min(extra_info_->galaxy_size_min_.x, extra->galaxy_size_min_.x),
				std::min(extra_info_->galaxy_size_min_.y, extra->galaxy_size_min_.y),
				std::min(extra_info_->galaxy_size_min_.z, extra->galaxy_size_min_.z));
			extra_info_->galaxy_size_max_ = glm::vec3(
				std::max(extra_info_->galaxy_size_max_.x, extra->galaxy_size_max_.x),
				std::max(extra_info_->galaxy_size_max_.y, extra->galaxy_size_max_.y),
				std::max(extra_info_->galaxy_size_max_.z, extra->galaxy_size_max_.z));

			const int count_stars = static_cast<int>(sun_list->size());
			for (int j = 0; j < count_stars; ++j)
			{
				(*sun_list)[j].Offset(center);
			}
			sun_lists_->insert(sun_lists_->end(), sun_list->begin(), sun_list->end());
		}

		std::static_pointer_cast<ClusterExtraInfo>(extra_info_)->basis_amount_ = count;
	}
}