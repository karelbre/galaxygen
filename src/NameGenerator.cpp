#include <NameGenerator.h>

#include <fstream>

namespace galaxy_generator
{
	/**
	 * Constructor
	 */
	NameGenerator::NameGenerator(std::shared_ptr<Random> rnd)
	{
		rnd_ = rnd;
		is_loaded_ = false;
	}

	/**
	 * Load name data from file
	 * @param filename input file
	 */
	bool NameGenerator::LoadNamesFromFile(std::string filename)
	{
		std::ifstream file;
		file.open(filename, std::ifstream::in);
		if (!file.is_open())
		{
			return false;
		}

		bool add_to_first_names = true;
		std::string line;
		getline(file, line);
		while (!line.empty())
		{
			if (strcmp(line.c_str(), ";") == 0)
			{
				add_to_first_names = false;
			}

			if (add_to_first_names)
			{
				first_names_.push_back(line);
			}
			else
			{
				second_names_.push_back(line);
			}
			line.clear();
			getline(file, line);
		}

		file.close();
		is_loaded_ = true;

		return true;
	}

	/**
	 * Get random generated name (first word + second word + number)
	 * @return Generated name
	 */
	std::string NameGenerator::GetName()
	{
		if (!is_loaded_)
		{
			return "";
		}

		std::string name;
		name.append(first_names_[rnd_->GetRandomUniformRange(0, static_cast<int>(first_names_.size()) - 1)]);
		name.append(second_names_[rnd_->GetRandomUniformRange(0, static_cast<int>(first_names_.size()) - 1)]);
		name.append(std::to_string(rnd_->GetRandomUniformRange(0, 99)));

		return name;
	}
}