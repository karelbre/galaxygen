// AUTHOR: Karel Brezina
// DATE: 5.10.2017

#include <Random.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 */
	Random::Random() : dis_uniform_{ 0.0f, 1.0f }
	{
		std::random_device rnd_device;
		rnd_generator_.seed(rnd_device());
	}

	/**
	 * Generate random number in range <0, 1> by uniform distribution
	 * @return Generated number
	 */
	float Random::GetRandomUniform()
	{
		return dis_uniform_(rnd_generator_);
	}

	/**
	 * Generate random number in defined range by uniform distribution
	 * @param min Minimal number in range
	 * @param max Maximal number in range
	 * @return Generated number
	 */
	int Random::GetRandomUniformRange(int min, int max)
	{
		return static_cast<int>(GetRandomUniformRange(static_cast<float>(min), static_cast<float>(max)));
	}

	/**
	 * Generate random number in defined range by uniform distribution
	 * @param min Minimal number in range
	 * @param max Maximal number in range
	 * @return Generated number
	 */
	float Random::GetRandomUniformRange(float min, float max)
	{
		const float range_max = max - min;
		return (GetRandomUniform() * range_max) + min;
	}

	/**
	 * Generate random number by normal distribution
	 * @param deviation value of normal distribution
	 * @param mean value of normal distribution
	 * @return Genereated number
	 */
	float Random::GetRandomNormal(float deviation, float mean)
	{
		if (deviation == 0.0f)
		{
			return mean;
		}

		std::normal_distribution<float> dis{ mean, deviation };
		return dis(rnd_generator_);
	}

	/**
	 * Generate random number in range by normal distribution
	 * @param deviation value of normal distribution
	 * @param mean value of normal distribution
	 * @param min Minimal number in range
	 * @param max Maximal number in range
	 * @return Generated number
	 */
	float Random::GetRandomNormalRange(float deviation, float mean, int min, int max)
	{
		return GetRandomNormalRange(deviation, mean, static_cast<float>(min), static_cast<float>(max));
	}

	/**
	 * Generate random number in range by normal distribution
	 * @param deviation value of normal distribution
	 * @param mean value of normal distribution
	 * @param min Minimal number in range
	 * @param max Maximal number in range
	 * @return Generated number
	 */
	float Random::GetRandomNormalRange(float deviation, float mean, float min, float max)
	{
		return std::max(std::min(GetRandomNormal(deviation, mean), max), min);
	}
}