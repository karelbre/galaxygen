﻿// AUTHOR: Karel Brezina
// DATE: 19.6.2017

#include <Heatmap.h>
#include <algorithm>

namespace galaxy_generator
{
	/**
	 * Constructor
	 */
	Heatmap::Heatmap(): width_(0), height_(0), pixels_(nullptr)
	{
		is_loaded_ = false;
	}

	/**
	 * Is heatmap initialized?
	 * @result Success
	 */
	bool Heatmap::IsLoaded() const
	{
		return is_loaded_;
	}

	/**
	 * Initialize heatmap structure
	 */
	void Heatmap::SetSourceImage(int width, int height, glm::vec4** pixels)
	{
		height_ = height;
		width_ = width;
		pixels_ = pixels;
		is_loaded_ = true;
	}

	/**
	 * Get width of loaded heatmap
	 * @return Heatmap width
	 */
	int Heatmap::GetWidth() const
	{
		return width_;
	}

	/**
	 * Get height of loaded heatmap
	 * @return Heatmap height
	 */
	int Heatmap::GetHeight() const
	{
		return height_;
	}

	/**
	 * Get probability of sun list temperature
	 * @param x 
	 * @param y
	 * @return Temperature probability
	 */
	float Heatmap::GetTempProb(int x, int y) const
	{
		if (pixels_ == nullptr)
		{
			return -1.0f;
		}

		return pixels_[y][x].r;
	}

	/**
	 * Get probability of sun list generate
	 * @param x
	 * @param y
	 * @return Generate probability
	 */
	float Heatmap::GetGenProb(int x, int y) const
	{
		if (pixels_ == nullptr)
		{
			return -1.0f;
		}

		return pixels_[y][x].g;
	}

	/**
	 * Create copy of loaded heatmap
	 * @return Created heatmap copy
	 */
	std::shared_ptr<Heatmap> Heatmap::GetCopy() const
	{
		if (pixels_ == nullptr)
		{
			return nullptr;
		}

		std::shared_ptr<Heatmap> copy = std::make_shared<Heatmap>();
		copy->width_ = width_;
		copy->height_ = height_;
		copy->pixels_ = new glm::vec4*[height_];

		for (int y = 0; y < height_; ++y)
		{
			copy->pixels_[y] = new glm::vec4[width_];
			for (int x = 0; x < width_; ++x)
			{
				copy->pixels_[y][x] = pixels_[y][x];
			}
		}

		return copy;
	}

	/**
	 * Erase generate probability from pixel to surround
	 * @param starting_point from where is counting radius
	 * @param radius distance of erase in pixels
	 * @return Success
	 */
	bool Heatmap::EraseProbAround(glm::ivec2 starting_point, int radius) const
	{
		if (pixels_ == nullptr)
		{
			return false;
		}

		const int minX = std::max(0, starting_point.x - radius);
		const int maxX = std::min(width_, starting_point.x + radius);
		const int minY = std::max(0, starting_point.y - radius);
		const int maxY = std::min(height_, starting_point.y + radius);

		for (int y = minY; y < maxY; ++y)
		{
			for (int x = minX; x < maxX; ++x)
			{
				pixels_[y][x] = glm::vec4();
			}
		}

		return true;
	}
}