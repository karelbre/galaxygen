// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#include <Spiral.h>
#include <Sphere.h>
#include <Cluster.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 * @param data settings of spiral shape
	 * @param rnd random class object
	 * @param name_gen name generator class object
	 */
	Spiral::Spiral(std::shared_ptr<SpiralData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen) 
		: BaseShape(data->temp_range_, rnd, name_gen)
	{
		SetData(data);
	}

	/**
	 * Set settings of spiral shape
	 */
	void Spiral::SetData(std::shared_ptr<SpiralData> data)
	{
		data_ = data;
	}

	/**
	 * Generate galaxy of spiral shape
	 */
	void Spiral::Generate()
	{
		sun_lists_->clear();
		const std::shared_ptr<SpiralData> sdata = std::static_pointer_cast<SpiralData>(data_);

		GenerateArms();
		GenerateCenter();
		GenerateBackgroundSunList();
	}

	/**
	 * Generate sun list galaxy surroundings
	 */
	void Spiral::GenerateBackgroundSunList() const
	{
		const std::shared_ptr<SpiralData> sdata = std::static_pointer_cast<SpiralData>(data_);
		Sphere sphere(std::make_shared<SphereData>(sdata->temp_range_, sdata->size_, 0.000001f, 0.0000001f, 0.35f, 0.125f, 0.35f), rnd_, name_gen_);
		sphere.Generate();
		std::shared_ptr<std::vector<SunList>> sun_list = sphere.GetSunList();
		sun_lists_->insert(sun_lists_->end(), sun_list->begin(), sun_list->end());
	}

	/**
	 * Generate spiral center of galaxy
	 */
	void Spiral::GenerateCenter() const
	{
		const std::shared_ptr<SpiralData> sdata = std::static_pointer_cast<SpiralData>(data_);
		Sphere sphere(std::make_shared<SphereData>(sdata->temp_range_, sdata->size_ * sdata->center_cluster_scale_, sdata->center_cluster_density_mean_,
			sdata->center_cluster_density_deviation_, sdata->center_cluster_scale_, sdata->center_cluster_scale_,
			sdata->center_cluster_scale_), rnd_, name_gen_);

		const float cluster_size = sdata->size_ * sdata->center_cluster_position_deviation_;
		Cluster cluster(std::make_shared<ClusterData>(sdata->temp_range_, &sphere, 0, sdata->center_cluster_count_deviation_,
			sdata->center_cluster_count_mean_, cluster_size, cluster_size, cluster_size), rnd_, name_gen_);

		cluster.Generate();
		std::shared_ptr<std::vector<SunList>> sun_list = cluster.GetSunList();

		const int count = static_cast<int>(sun_list->size());
		for (int i = 0; i < count; ++i)
		{
			(*sun_list)[i].Swirl(glm::vec3(0, 1, 0), sdata->swirl_ * 5);
		}
		sun_lists_->insert(sun_lists_->end(), sun_list->begin(), sun_list->end());
		// Compute AABB borders
		const std::shared_ptr<BaseExtraInfo> extra = cluster.GetExtraInfo();
		extra_info_->galaxy_size_min_ = glm::vec3(
			std::min(extra_info_->galaxy_size_min_.x, extra->galaxy_size_min_.x),
			std::min(extra_info_->galaxy_size_min_.y, extra->galaxy_size_min_.y),
			std::min(extra_info_->galaxy_size_min_.z, extra->galaxy_size_min_.z));
		extra_info_->galaxy_size_max_ = glm::vec3(
			std::max(extra_info_->galaxy_size_max_.x, extra->galaxy_size_max_.x),
			std::max(extra_info_->galaxy_size_max_.y, extra->galaxy_size_max_.y),
			std::max(extra_info_->galaxy_size_max_.z, extra->galaxy_size_max_.z));
	}

	/**
	 * Generate spiral arms of galaxy
	 */
	void Spiral::GenerateArms() const
	{
		Random rnd;
		const std::shared_ptr<SpiralData> sdata = std::static_pointer_cast<SpiralData>(data_);
		const int arms = rnd.GetRandomUniformRange(sdata->minimum_arms_, sdata->maximum_arms_);
		const float arm_angle = static_cast<float>((M_PI * 2) / arms);

		const float cls_scale_dev = sdata->arm_cluster_scale_deviation_ * sdata->size_;
		const float cls_scale_min = sdata->min_arm_cluster_scale_ * sdata->size_;
		const float cls_scale_max = sdata->max_arm_cluster_scale_ * sdata->size_;

		const int max_clusters = static_cast<int>((sdata->size_ / sdata->spacing_) / arms);
		for (int arm = 0; arm < arms; ++arm)
		{
			const int clusters = static_cast<int>(round(rnd.GetRandomNormal(max_clusters * sdata->cluster_count_deviation_, static_cast<float>(max_clusters))));
			for (int i = 0; i < clusters; ++i)
			{
				float angle = rnd.GetRandomNormal(0.5f * arm_angle * sdata->cluster_center_deviation_, 0.0f) + arm_angle * arm;

				const float dist = abs(rnd.GetRandomNormal(sdata->size_ * 0.4f, 0.0f));
				const glm::vec3 center = glm::angleAxis(angle, glm::vec3(0, 1, 0)) * glm::vec3(0, 0, dist);
				const float c_size = rnd.GetRandomNormalRange(cls_scale_dev, cls_scale_min * 0.5f + cls_scale_max * 0.5f, cls_scale_min, cls_scale_max);

				Sphere sphere(std::make_shared<SphereData>(sdata->temp_range_, c_size, 0.00025f, 0.000001f, 1.0f, 1.0f, 1.0f), rnd_, name_gen_);
				sphere.Generate();
				std::shared_ptr<std::vector<SunList>> sun_list = sphere.GetSunList();

				const int max_sun_list = static_cast<int>(sun_list->size());
				for (int j = 0; j < max_sun_list; ++j)
				{
					(*sun_list)[j].Offset(center);
					(*sun_list)[j].Swirl(glm::vec3(0, 1, 0), sdata->swirl_);
				}
				sun_lists_->insert(sun_lists_->end(), sun_list->begin(), sun_list->end());
				// Compute AABB borders 
				const std::shared_ptr<BaseExtraInfo> extra = sphere.GetExtraInfo();
				extra_info_->galaxy_size_min_ = glm::vec3(
					std::min(extra_info_->galaxy_size_min_.x, extra->galaxy_size_min_.x),
					std::min(extra_info_->galaxy_size_min_.y, extra->galaxy_size_min_.y),
					std::min(extra_info_->galaxy_size_min_.z, extra->galaxy_size_min_.z));
				extra_info_->galaxy_size_max_ = glm::vec3(
					std::max(extra_info_->galaxy_size_max_.x, extra->galaxy_size_max_.x),
					std::max(extra_info_->galaxy_size_max_.y, extra->galaxy_size_max_.y),
					std::max(extra_info_->galaxy_size_max_.z, extra->galaxy_size_max_.z));
			}
		}
	}
}