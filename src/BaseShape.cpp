#include <BaseShape.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 * @param temp_range range of sun list temperature
	 * @param rnd random class
	 * @param name_gen name generator class
	 */
	BaseShape::BaseShape(glm::vec2 temp_range, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen)
	{
		sun_lists_ = std::make_shared<std::vector<SunList>>();
		rnd_ = rnd;
		name_gen_ = name_gen;
		data_ = std::make_shared<BaseShapeData>(temp_range);
		extra_info_ = std::make_shared<BaseExtraInfo>();
	}

	/**
	 * Get generated sun list
	 * @return Sun list
	 */
	std::shared_ptr<std::vector<SunList>> BaseShape::GetSunList() const
	{
		return sun_lists_;
	}

	/**
	 * Get extra info after generating process is done
	 * @see ClusterExtraInfo
	 * @return Structure with extra info
	 */
	std::shared_ptr<BaseExtraInfo> BaseShape::GetExtraInfo() const
	{
		return extra_info_;
	}

	/**
	 * Set internal variables needed to work (if it wasn't specified in constructor params)
	 * @see BaseShape
	 * @param temp_range range of sun list temperature
	 * @param rnd random class
	 * @param name_gen name generator class
	 */
	void BaseShape::SetVars(glm::vec2 temp_range, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen)
	{
		rnd_ = rnd;
		name_gen_ = name_gen;
		data_->temp_range_ = temp_range;
	}
}