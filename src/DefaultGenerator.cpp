// AUTHOR: Karel Brezina
// DATE: 25.11.2017

#include <DefaultGenerator.h>

namespace galaxy_generator
{
	/*
	 * Constructor
	 */
	DefaultGenerator::DefaultGenerator(std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen) :
		sphere_(std::make_shared<SphereData>(temp_range_, 100.0f, 0.00025f, 0.000001f, 1.0f, 1.0f, 1.0f), rnd, name_gen),
		grid_(std::make_shared<GridData>(temp_range_, 5.0f, 2.0f), rnd, name_gen),
		spiral_(std::make_shared<SpiralData>(temp_range_), rnd, name_gen),
		cluster_(std::make_shared<ClusterData>(temp_range_, &sphere_, 0), rnd, name_gen)
	{
		name_gen_ = name_gen;
		name_gen->LoadNamesFromFile(GalaxyGen_RESOURCES + std::string("Names.txt"));
		rnd_ = rnd;
		heatmap_extra_info_ = std::make_shared<BaseExtraInfo>();
	}

	/**
	 * Generate galaxy of specified shape
	 * @param type requested galaxy shape
	 * @return Generate sun list
	 */
	std::shared_ptr<std::vector<SunList>> DefaultGenerator::Generate(galaxy_type type)
	{
		BaseShape* generator;
		switch (type)
		{
		case SPIRAL:
			generator = &spiral_;
			break;

		case CLUSTER:
			generator = &cluster_;
			break;

		case GRID:
			generator = &grid_;
			break;

		case SPHERE:
			generator = &sphere_;
			break;

		default:
			return nullptr;
		}

		generator->Generate();
		return generator->GetSunList();
	}

	/**
	 * Generate galaxy by heatmap
	 * @param gen_data Additional generate info
	 * @return Generated sun list
	 */
	std::shared_ptr<std::vector<SunList>> DefaultGenerator::Generate(const HeatmapGenData& gen_data)
	{
		const std::shared_ptr<Heatmap> heatmap = gen_data.heatmap_;
		if ((heatmap != nullptr) && !heatmap->IsLoaded())
		{
			return nullptr;
		}

		std::shared_ptr<std::vector<SunList>> sun_list_vec = std::make_shared<std::vector<SunList>>();
		const std::shared_ptr<Heatmap> map = heatmap->GetCopy();

		const float galaxy_size_x = gen_data.galaxy_size_.x;
		const float galaxy_size_y_half = gen_data.galaxy_size_.y / 2.0f;
		const float galaxy_size_z = gen_data.galaxy_size_.z;

		const int maxY = map->GetHeight();
		const int maxX = map->GetWidth();
		for (int y = 0; y < maxY; ++y)
		{
			for (int x = 0; x < maxX; ++x)
			{
				const float slotProb = map->GetGenProb(x, y);
				if (slotProb < 0.01f)
				{
					// No chance to generate
					continue;
				}

				if (rnd_->GetRandomUniform() > slotProb)
				{
					// Bad luck
					continue;
				}

				// Compute position and temperature of sun list
				const float offsetY = std::min(std::max(rnd_->GetRandomNormal(gen_data.height_deviation_, 0.0f), -galaxy_size_y_half), galaxy_size_y_half);
				const glm::vec2 temp_range = gen_data.temp_range_;
				const float temp_mean = temp_range.x + map->GetTempProb(x, y) * (temp_range.y - temp_range.x);
				const float temp = std::max(temp_range.x, std::min(temp_range.y, rnd_->GetRandomNormal(gen_data.temp_deviation_, temp_mean)));
				SunList sun_list{ glm::vec3((x / static_cast<float>(maxX)) * galaxy_size_x, offsetY, (y / static_cast<float>(maxY)) * galaxy_size_z), name_gen_->GetName(), temp };
				sun_list_vec->emplace_back(sun_list);
				map->EraseProbAround(glm::vec2(x, y), gen_data.minimal_distance_);

				const glm::vec3 position = sun_list.GetPosition();
				heatmap_extra_info_->galaxy_size_min_ = glm::vec3(
					std::min(heatmap_extra_info_->galaxy_size_min_.x, position.x),
					std::min(heatmap_extra_info_->galaxy_size_min_.y, position.y),
					std::min(heatmap_extra_info_->galaxy_size_min_.z, position.z));
				heatmap_extra_info_->galaxy_size_max_ = glm::vec3(
					std::max(heatmap_extra_info_->galaxy_size_max_.x, position.x),
					std::max(heatmap_extra_info_->galaxy_size_max_.y, position.y),
					std::max(heatmap_extra_info_->galaxy_size_max_.z, position.z));
			}
		}

		return sun_list_vec;
	}

	/**
	 * Set generate attributes for specified galaxy shape
	 * @param data attributes of specified type
	 * @return Success of set
	 */
	bool DefaultGenerator::SetGeneratorSettings(std::shared_ptr<BaseShapeData> data)
	{
		if (data->data_type_ == CLUSTER)
		{
			const std::shared_ptr<ClusterData> cdata = std::static_pointer_cast<ClusterData>(data);
			cluster_.SetData(cdata);
		}
		else if (data->data_type_ == SPHERE)
		{
			const std::shared_ptr<SphereData> sdata = std::static_pointer_cast<SphereData>(data);
			sphere_.SetData(sdata);
		}
		else if (data->data_type_ == GRID)
		{
			const std::shared_ptr<GridData> gdata = std::static_pointer_cast<GridData>(data);
			grid_.SetData(gdata);
		}
		else if (data->data_type_ == SPIRAL)
		{
			const std::shared_ptr<SpiralData> sdata = std::static_pointer_cast<SpiralData>(data);
			spiral_.SetData(sdata);
		}
		else
		{
			// Not defined galaxy shape
			return false;
		}

		return true;
	}

	/**
	 * Get extra info after generating is done
	 * @param type requested galaxy shape
	 * @return Extra info
	 */
	std::shared_ptr<BaseExtraInfo> DefaultGenerator::GetExtraInfo(galaxy_type type)
	{
		switch (type)
		{
			case UNKNOWN:
				return heatmap_extra_info_;

			case SPIRAL:
				return spiral_.GetExtraInfo();

			case SPHERE:
				return sphere_.GetExtraInfo();

			case GRID:
				return grid_.GetExtraInfo();

			case CLUSTER:
				return cluster_.GetExtraInfo();

			default:
				return nullptr;
		}
	}
}