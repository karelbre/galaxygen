// AUTHOR: Karel Brezina
// DATE: 24.9.2017

#include <Grid.h>

namespace galaxy_generator
{
	/**
	 * Constructor
	 * @param data settings of grid shape
	 * @param rnd random class object
	 * @param name_gen name generator class object
	 */
	Grid::Grid(std::shared_ptr<GridData> data, std::shared_ptr<Random> rnd, std::shared_ptr<NameGenerator> name_gen) 
		: BaseShape(data->temp_range_, rnd, name_gen)
	{
		SetData(data);
	}

	/**
	 * Set data variables to internal storage
	 * @param data set data
	 */
	void Grid::SetData(std::shared_ptr<GridData> data)
	{
		data_ = data;
	}

	/**
	 * Generate sun list in grid shape
	 */
	void Grid::Generate()
	{
		sun_lists_->clear();

		const std::shared_ptr<GridData> gdata = std::static_pointer_cast<GridData>(data_);
		const int count = static_cast<int>(gdata->size_ / gdata->spacing_);
		for (int i = 0; i < count; ++i)
		{
			for (int j = 0; j < count; ++j)
			{
				for (int k = 0; k < count; ++k)
				{
					SunList sun_list(glm::vec3(i * gdata->spacing_, j * gdata->spacing_, k * gdata->spacing_),
						name_gen_->GetName(), rnd_->GetRandomUniformRange(gdata->temp_range_.x, gdata->temp_range_.y));
					const float offset = -gdata->size_ / 2;
					sun_list.Offset(glm::vec3(offset, offset, offset));
					sun_lists_->emplace_back(sun_list);
					// Compute AABB borders
					const glm::vec3 sun_list_pos = sun_list.GetPosition();
					extra_info_->galaxy_size_min_ = glm::vec3(
						std::min(extra_info_->galaxy_size_min_.x, sun_list_pos.x),
						std::min(extra_info_->galaxy_size_min_.y, sun_list_pos.y),
						std::min(extra_info_->galaxy_size_min_.z, sun_list_pos.z));
					extra_info_->galaxy_size_max_ = glm::vec3(
						std::max(extra_info_->galaxy_size_max_.x, sun_list_pos.x),
						std::max(extra_info_->galaxy_size_max_.y, sun_list_pos.y),
						std::max(extra_info_->galaxy_size_max_.z, sun_list_pos.z));
				}
			}
		}
	}
}
